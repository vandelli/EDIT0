
#include <boost/asio.hpp>
#include <boost/noncopyable.hpp>
#include "tbb/concurrent_queue.h"
#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <atomic>
#include <algorithm>
#include <sys/socket.h>
#include <errno.h>
#include <poll.h>
#include <string.h>
#include <csignal>

using boost::asio::ip::tcp;

class Message {

public:
  Message()
  {}
  
  Message(const char * msg, unsigned int idx):
    data(msg),
    index(idx)
  {}

public:
  std::string data;
  unsigned int index;  
};


class Builder : boost::noncopyable {

public:
  static Builder& instance() {
    static Builder instance;
    return instance;
  }

  void run() {
    m_thread = std::make_shared<std::thread>(&Builder::work, this);
  }

  void stop() {

    m_running = false;

    m_thread->join();
  }

  void add_input(tcp::socket socket) {

    // Add a new input connection to the list
    // of connections
    m_inputs.push_back(std::move(socket));
  }
  
  int n_inputs() {
    return m_inputs.size();
  }

  
private:
  
  Builder():
    m_running(false)
  {}

  bool receive(tcp::socket & socket, Message & message) {

    int poll_timeout_ms = 1;

    // Descriptor for the poll operation
    // Instructs poll to check for available data on
    // on the socket
    struct pollfd pollfd;
    memset(&pollfd, 0, sizeof(pollfd));
    
    pollfd.fd = socket.native_handle();
    pollfd.events |= POLLIN;

    while(true) {
      
      // With poll we check if data are available
      // before reading
      // This is needed to avoid blocking forever
      auto result = poll(&pollfd, 1, poll_timeout_ms);
      if (result == 0) {
        // No data available
        continue;
      } else if (result < 0) {
        // Some error occourred (e.g. connection was closed)
        int err_code = errno;
        std::cout << "Something bad happened "
                  << ::strerror(err_code) << std::endl;
      } else {
        // Data are available
        break;
      }
    }

    // The socket has data, let's receive the message
    try{
      // Receive the size of message
      unsigned int length;
      boost::asio::read(socket, boost::asio::buffer(&length, sizeof(length)));
      
      // Receive the sequence index
      boost::asio::read(socket, boost::asio::buffer(&message.index,
                                                    sizeof(message.index)));
            
      // Allocate the needed memory
      std::unique_ptr<char[]> buffer(new char[length]);
      
      // Receive the full message
      auto recv =
        boost::asio::read(socket, boost::asio::buffer(buffer.get(), length));

      message.data = std::string(buffer.get(), recv);
            
    } catch (boost::system::system_error &e) {
      std::cout << "Something bad happened " << e.what() << std::endl;
      return false;
    }
            
    return true;
  }

  
  void work() {

    m_running = true;

    while (m_running) {
      
      auto it = m_inputs.begin();
      std::string built;

      // We loop over all the inputs
      // and read one message for each of them
      // The message are assembled into a built message

      unsigned int index;
      
      while (it != m_inputs.end()) {
        Message tmp;

        // Receive the next message
        bool success = this->receive(*it, tmp);
        
        if (success) {
          // Initialize index value from the first input
          if (it == m_inputs.begin()) {
            index = tmp.index;
          }

          // Assemble the message
          if (tmp.index == index) {
            built += tmp.data;
          } else {
            std::cerr << "Message with wrong index. Expected: " << index
                      << " Received: " << tmp.index << std::endl;
          }

          // Next channel
          ++it;
        } else {
          // Something went wrong.
          // We remove this input from the list
          it = m_inputs.erase(it);
        }
      }

      if (m_inputs.size()) {
        std::cout << "Message built from " << m_inputs.size()
                  << " inputs. Index: "
                  << index
                  << " Message: " << built << std::endl;
      }
    }
  }
  
private:
  std::atomic<bool> m_running;
  std::vector<tcp::socket> m_inputs;
  std::shared_ptr<std::thread> m_thread;
};


class DataServer {

public:
  DataServer():
    m_io_service(),
    m_acceptor(m_io_service),
    m_running(false)
  {
    // Port 12345 is our listening point
    tcp::endpoint endpoint(tcp::v4(), 12345);
    m_acceptor.open(endpoint.protocol());

    // Black magic to avoid errors if a new 
    // instance is started immediately after a previous one
    // (I will be happy to explain this, but not to type it here)
    m_acceptor.set_option(tcp::acceptor::reuse_address(true));

    // Listen for incoming connections
    m_acceptor.bind(endpoint);
  }

  
  void run() {
    m_acceptor.listen();
    m_thread = std::make_shared<std::thread>(&DataServer::work, this);
  }
  
  void stop() {
    // Stop accepting new connections
    m_running = false;

    m_thread->join();

    m_acceptor.close();
  }
  
private:
  
  void work() {
  
    // Timeout for poll operation
    int poll_timeout_ms = 1;

    // Descriptor for the poll operation
    // Instructs poll to check for available data on
    // on the acceptor socket
    struct pollfd pollfd;
    memset(&pollfd, 0, sizeof(pollfd));
    
    pollfd.fd = m_acceptor.native_handle();
    pollfd.events |= POLLIN;

    m_running = true;
    
    while(m_running) {
      
      // With poll we check if data are available
      // before reading
      // This is needed to avoid blocking forever
      if (poll(&pollfd, 1, poll_timeout_ms) > 0) {

        // Data are available. This is the server
        // so we expect this is a new incoming connection
        // We accept it and provide it to the builder
        tcp::socket socket(m_io_service);
        m_acceptor.accept(socket);
        Builder::instance().add_input(std::move(socket));
      }
    }
  }

private:
  boost::asio::io_service m_io_service;
  tcp::acceptor m_acceptor;
  std::atomic<bool> m_running;
  std::shared_ptr<std::thread> m_thread;
};


bool running;

void signal_handler(int) {
  running = false;
}

int main() {

  running = true;
  std::signal(SIGTERM, signal_handler);

  auto& builder = Builder::instance();
  DataServer server;
  server.run();

  int wait_sec = 10;
  std::cout << "Server is running. You have " << wait_sec << " seconds to connect the clients. I won't accept new input afterwards." << std::endl;
  
  ::sleep(wait_sec);

  server.stop();

  std::cout << "Server stopped, builder starting with " << builder.n_inputs()
            << " inputs." << std::endl;

  builder.run();
  
  while(running) {
    ::usleep(100000);
  }

  builder.stop();
  
  return 0;
}
