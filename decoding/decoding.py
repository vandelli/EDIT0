#!/usr/bin/env python2


import sys
import struct
from operator import itemgetter
import math

def parse_file(filename):

    with open(filename) as datafile:

        events = []
        
        while True:
           
            fmt = 'III'
            tmp = datafile.read(struct.calcsize(fmt))
            if not tmp:
                break
            
            marker, _, datahead = \
                struct.unpack_from(fmt, tmp)
            
            if marker != 0xAABBCCDD:
                print 'Wrong event marker 0x%08x' % marker
                break
        
            num_data_words = (datahead >>8) & 0x3f
            fmt = 'I' * num_data_words

            data = \
                struct.unpack_from(fmt, datafile.read(struct.calcsize(fmt)))

            events.append([((d >> 16) & 0x1f, d &0xfff) for d in data])

            # Trailer
            fmt = 'I'
            _ = datafile.read(struct.calcsize(fmt))

    return events
            

if __name__ == '__main__':
    
    events = parse_file(sys.argv[1])

    # sort pair by channel
    events = [sorted(e, key=itemgetter(0)) for e in events]

    # remove events with single hit
    events = [e for e in events if len(e) > 1]

    # compute difference
    events = (math.abs(e[0][1]-e[1][1]) for e in events)

    print list(events)
