
#include <boost/asio.hpp>
#include <boost/noncopyable.hpp>
#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <atomic>
#include <algorithm>
#include <sys/socket.h>
#include <errno.h>
#include <poll.h>
#include <csignal>

using boost::asio::ip::tcp;

class Builder : boost::noncopyable {

public:
  static Builder& instance() {
    static Builder instance;
    return instance;
  }

  void run() {
    m_thread = std::make_shared<std::thread>(&Builder::work, this);
  }

  void stop() {

    m_running = false;

    m_thread->join();
  }

  void add_input(tcp::socket socket) {

    // Add a new input connection to the list
    // of newly received connections
    std::unique_lock<std::mutex> lock(m_mutex);
    m_new_inputs.push_back(std::move(socket));
  }
  
private:
  
  Builder():
    m_running(false)
  {}

  bool receive(tcp::socket & socket, std::string & message) {

    int poll_timeout_ms = 1;

    // Descriptor for the poll operation
    // Instructs poll to check for available data on
    // on the socket
    struct pollfd pollfd;
    memset(&pollfd, 0, sizeof(pollfd));
    pollfd.fd = socket.native_handle();
    pollfd.events |= POLLIN;

    while(true) {
      
      // With poll we check if data are available
      // before reading
      // This is needed to avoid blocking forever
      auto result = poll(&pollfd, 1, poll_timeout_ms);
      if (result == 0) {
        // No data available
        continue;
      } else if (result < 0) {
        // Some error occourred (e.g. connection was closed)
        int err_code = errno;
        std::cout << "Something bad happened "
                  << ::strerror(err_code) << std::endl;
      } else {
        // Data are available
        break;
      }
    }

    // The socket has data, let's receive the message
    try{
      // Receive the size of message
      unsigned int length;
      boost::asio::read(socket, boost::asio::buffer(&length, sizeof(length)));
            
      // Allocate the needed memory
      std::unique_ptr<char[]> buffer(new char[length]);
      
      // Receive the full message
      auto recv =
        boost::asio::read(socket, boost::asio::buffer(buffer.get(), length));

      message = std::string(buffer.get(), recv);
            
    } catch (boost::system::system_error &e) {
      std::cout << "Something bad happened " << e.what() << std::endl;
      return false;
    }
            
    return true;
    
  }

  
  void work() {

    m_running = true;

    while (m_running) {

      // First check if new connections
      if (!m_new_inputs.empty()) {
        std::unique_lock<std::mutex> lock(m_mutex);
        std::move(m_new_inputs.begin(), m_new_inputs.end(),
                  std::back_inserter(m_inputs));
        m_new_inputs.clear();
      }
      
      auto it = m_inputs.begin();
      std::string built;

      // We loop over all the inputs
      // and read one message for each of them
      // The message are assembled into a built message

      while (it != m_inputs.end()) {
        std::string tmp;

        // Receive the next message
        bool success = this->receive(*it, tmp);
        
        if (success) {
          // Assemble the new part
          built += tmp;
          ++it;
        } else {
          // Something went wrong.
          // We remove this input from the list
          it = m_inputs.erase(it);
        }
      }

      if (m_inputs.size()) {
        std::cout << "Message built from " << m_inputs.size()
                  << " inputs: " << built << std::endl;
      }
    }
  }
  
private:
  std::atomic<bool> m_running;
  std::vector<tcp::socket> m_new_inputs;
  std::vector<tcp::socket> m_inputs;
  std::mutex m_mutex;
  std::shared_ptr<std::thread> m_thread;
};


class DataServer {

public:
  DataServer():
    m_io_service(),
    m_acceptor(m_io_service),
    m_running(false)
  {
    // Port 12345 is our listening point
    tcp::endpoint endpoint(tcp::v4(), 12345);
    m_acceptor.open(endpoint.protocol());

    // Black magic to avoid errors if a new 
    // instance is started immediately after a previous one
    // (I will be happy to explain this, but not to type it here)
    m_acceptor.set_option(tcp::acceptor::reuse_address(true));

    // Listen for incoming connections
    m_acceptor.bind(endpoint);
    m_acceptor.listen();
  }

  
  void run() {
    m_thread = std::make_shared<std::thread>(&DataServer::work, this);
  }
  
  void stop() {
    // Stop accepting new connections
    m_running = false;

    m_thread->join();
  }
  
private:
  
  void work() {
  
    // Timeout for poll operation
    int poll_timeout_ms = 1;

    // Descriptor for the poll operation
    // Instructs poll to check for available data on
    // on the acceptor socket
    struct pollfd pollfd;
    memset(&pollfd, 0, sizeof(pollfd));
    
    pollfd.fd = m_acceptor.native_handle();
    pollfd.events |= POLLIN;

    m_running = true;
    
    while(m_running) {
      
      // With poll we check if data are available
      // before reading
      // This is needed to avoid blocking forever
      if (poll(&pollfd, 1, poll_timeout_ms) > 0) {

        // Data are available. This is the server
        // so we expect this is a new incoming connection
        // We accept it and provide it to the builder
        tcp::socket socket(m_io_service);
        m_acceptor.accept(socket);
        Builder::instance().add_input(std::move(socket));
      }
    }

    std::cout << "Server done" << std::endl;
  }

private:
  boost::asio::io_service m_io_service;
  tcp::acceptor m_acceptor;
  std::atomic<bool> m_running;
  std::shared_ptr<std::thread> m_thread;
};


bool running;

void signal_handler(int) {
  running = false;
}

int main() {

  running = true;
  std::signal(SIGTERM, signal_handler);

  auto& builder = Builder::instance();
  builder.run();
  
  DataServer server;
  server.run();

  while(running) {
    ::usleep(100000);
  }

  server.stop();

  builder.stop();
  
  return 0;
}
