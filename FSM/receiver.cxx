
#include <boost/asio.hpp>
#include <boost/noncopyable.hpp>
#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <atomic>
#include <algorithm>
#include <sys/socket.h>
#include <errno.h>
#include <poll.h>
#include <string.h>
#include <csignal>
#include <fstream>
#include <list>
#include <unordered_map>

using boost::asio::ip::tcp;

class Controlled {

public:
  virtual ~Controlled(){}
  virtual void configure(){}
  virtual void unconfigure(){}
  virtual void start(){}
  virtual void stop(){}
};

class FSM : boost::noncopyable {

public:
  static FSM& instance() {
    static FSM instance;
    return instance;
  }

  enum class State {NONE, CONFIGURED, RUNNING};

  std::map<State, std::string> StateStrings
  { {State::NONE, "NONE"},
    {State::CONFIGURED, "CONFIGURED"},
    {State::RUNNING, "RUNNING"}
  };
  
  enum class Command {CONFIGURE, UNCONFIGURE, START, STOP,};

  std::unordered_map<std::string, Command> CommandMap
  {{"configure", Command::CONFIGURE},
   {"unconfigure", Command::UNCONFIGURE},
   {"start", Command::START},
   {"stop", Command::STOP}
  };

  std::map<Command, State> ValidStates
  {
    {Command::CONFIGURE, State::NONE},
    {Command::UNCONFIGURE, State::CONFIGURED},
    {Command::START, State::CONFIGURED},
    {Command::STOP, State::RUNNING},
  };
  
  virtual ~FSM(){}
  
private:
  FSM():
    m_running(false)
  {
    std::signal(SIGINT, FSM::signal_handler);
  }

public:

  void add_control(std::list<std::shared_ptr<Controlled>>& ctrls) {
    if (!m_running) {
      std::copy(ctrls.begin(), ctrls.end(),
                std::back_inserter(m_ctrls));

    }
  }

  void add_control(std::shared_ptr<Controlled> ctrl) {
    if (!m_running) {
      m_ctrls.push_back(ctrl);
    }
  }

  void run() {

    m_running = true;
    m_state = State::NONE;
    
    const int buffer_size = 8192;
    char command_buffer[buffer_size];
    boost::asio::io_service io_service;
    tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), 4444)); 
    
    // Timeout for poll operation
    int poll_timeout_ms = 10;

    // Descriptor for the poll operation
    // Instructs poll to check for available data on
    // on the acceptor socket
    struct pollfd acceptor_pollfd;
    memset(&acceptor_pollfd, 0, sizeof(pollfd));
    
    acceptor_pollfd.fd = acceptor.native_handle();
    acceptor_pollfd.events |= POLLIN;

    while (m_running) {

      tcp::socket socket(io_service);
      // With poll we check if data are available
      // before reading
      // This is needed to avoid blocking forever
      if (poll(&acceptor_pollfd, 1, poll_timeout_ms) > 0) {

        // Data are available. This is the server
        // so we expect this is a new incoming connection
        // We accept it and provide it to the builder
        acceptor.accept(socket);
      } else {
        continue;
      }

      struct pollfd pollfd;
      memset(&pollfd, 0, sizeof(pollfd));
      
      pollfd.fd = socket.native_handle();
      pollfd.events |= POLLIN;

      std::cout << "Control connection received" << std::endl;
      
      while (m_running) {
        //Wait for data to be ready
        auto result = poll(&pollfd, 1, poll_timeout_ms);
        if (result < 0) {
          // Connection problem
          break;
        } else if (result == 0) {
          continue;
        }

        // We have data to read!
        size_t recv_size = 0;
        try {
          recv_size =
            socket.read_some(boost::asio::buffer(&command_buffer, buffer_size));
        } catch (boost::system::system_error &e) {
          break;
        }

        std::string cmd(command_buffer, recv_size);
        this->fsm(cmd);
      }
    }

    std::cout << "FSM done" << std::endl;
    
  }
  
private:

  void fsm(const std::string & cmd) {

    Command command;
    try {
      command = CommandMap.at(cmd);
    } catch(std::out_of_range &e) {
      std::cerr << "Unknown command: " << cmd
                << " in state "
                << StateStrings.at(m_state) << std::endl;
      return;
    }

    if (m_state != ValidStates[command]) {
      std::cerr << "Invalid command: " << cmd
                << " in state "
                << StateStrings.at(m_state) << std::endl;
      return;
    }

    if (command == Command::CONFIGURE) {

      for(auto& ctrl: m_ctrls) {
        ctrl->configure();
      }
      
      m_state = State::CONFIGURED;
    } else if (command == Command::START) {

      for(auto& ctrl: m_ctrls) {
        ctrl->start();
      }
      
      m_state = State::RUNNING;
    } else if (command == Command::STOP) {

      for(auto& ctrl: m_ctrls) {
        ctrl->stop();
      }
      
      m_state = State::CONFIGURED;
    } else if (command == Command::UNCONFIGURE) {

      for(auto& ctrl: m_ctrls) {
        ctrl->unconfigure();
      }
      
      m_state = State::NONE;
    }

    std::cout << "Command " << cmd << " New state " << StateStrings.at(m_state)<< std::endl;
  }

  static void signal_handler(int) {
    FSM::instance().stop();
  }

  void stop() {
    m_running = false;
  }
  
private:
  std::list<std::shared_ptr<Controlled>> m_ctrls;  
  std::atomic<bool> m_running;
  State m_state;
};



class Message {

public:
  std::unique_ptr<uint8_t[]> data;
  unsigned int size_b;
  unsigned int index;
};


class Built {

public:
  Built(){}
  
  void append(Message m) {
    fragments.emplace_back(std::move(m));
  }

public:
  std::vector<Message> fragments;
};

class Builder : public Controlled {

public:
  Builder():
    m_running(false)
  {}

  virtual ~Builder(){}
  
  void start() {
    m_thread = std::make_shared<std::thread>(&Builder::work, this);
  }

  void stop() {

    // Give time to the clients to stop and finish
    // sending all data
    ::sleep(1);
      
    m_running = false;
    m_thread->join();
    m_events.clear();
  }

  void unconfigure() {
    m_inputs.clear();
  }

  
  void add_input(tcp::socket socket) {
    // Add a new input connection to the list
    // of connections
    m_inputs.push_back(std::move(socket));
  }
    
private:
  
  bool receive(tcp::socket & socket, Message & message) {

    int poll_timeout_ms = 1;

    // Descriptor for the poll operation
    // Instructs poll to check for available data on
    // on the acceptor socket
    struct pollfd pollfd;
    memset(&pollfd, 0, sizeof(pollfd));
    
    pollfd.fd = socket.native_handle();
    pollfd.events |= POLLIN;
        
    while(m_running) {
      
      // With poll we check if data are available
      // before reading
      // This is needed to avoid blocking forever
      auto result = poll(&pollfd, 1, poll_timeout_ms);
      if (result == 0) {
        // No data available
        continue;
      } else if (result < 0) {
        // Some error occourred (e.g. connection was closed)
        int err_code = errno;
        std::cout << "Something bad happened "
                  << ::strerror(err_code) << std::endl;
        return false;
      } else {
        // Data are available
        break;
      }
    }

    if (!m_running) {
      return false;
    }
    
    // The socket has data, let's receive the message
    try{
      // Receive the size of message
      unsigned int length;
      boost::asio::read(socket, boost::asio::buffer(&length, sizeof(length)));
      
      // Receive the sequence index
      boost::asio::read(socket, boost::asio::buffer(&message.index,
                                                    sizeof(message.index)));
            
      // Allocate the needed memory
      std::unique_ptr<uint8_t[]> buffer(new uint8_t[length]);
      
      // Receive the full message
      boost::asio::read(socket, boost::asio::buffer(buffer.get(), length));

      message.data = std::move(buffer);
      message.size_b = length;
            
    } catch (boost::system::system_error &e) {
      std::cout << "Something bad happened " << e.what() << std::endl;
      return false;
    }
        
    return true;
  }
 
  void work() {

    unsigned int events = 0;
    
    std::string filename = "run" + std::to_string(::time(nullptr)) + ".dat";

    std::fstream fs;
    fs.open(filename, std::ios::out | std::ios::binary | std::ios::trunc);
      
    m_running = true;

    while (m_running) {
         
      // We loop over all the inputs
      // and read one message for each of them
      // The message are assembled into a built message

      auto it = m_inputs.begin();
      while (it != m_inputs.end()) {
        Message tmp;
        
        // Receive the next message
        bool success = this->receive(*it, tmp);
        
        if (!m_running) {
          break;
        }
        
        if (success) {

          auto built_it = m_events.find(tmp.index);
          if (built_it == m_events.end()) {
            // This is a new index
            auto inserted =
              m_events.emplace(tmp.index, Built{});
            // This if the iterator to the inserted objetct
            built_it = inserted.first;
          } 

          // Add the data to the building object
          built_it->second.append(std::move(tmp));
          
          // Check if the message is complete
          if (built_it->second.fragments.size() == m_inputs.size()) {
            // Message is complete, we write it out

            // Write a start of event first
            fs.write(reinterpret_cast<const char *>(&Builder::event_head),
                     sizeof(Builder::event_head));
            
            // Write the index
            fs.write(reinterpret_cast<const char *>(&built_it->first),
                     sizeof(built_it->first));
            
            // Iterate over the fragments and write them
            auto it = built_it->second.fragments.begin();
            const auto end = built_it->second.fragments.end();

            for (; it != end; ++it) {
              fs.write(reinterpret_cast<const char *>(it->data.get()),
                       it->size_b);              
            }

            fs.flush();
            // We can now delete the built message
            m_events.erase(built_it);
            events++;
            if (events % 10 == 0) {
              std::cout << "Event built: " << events << std::endl;
            }
          }
          
          // Next channel
          ++it;
        } else {
          // Something went wrong.
          // We remove this input from the list
          it = m_inputs.erase(it);
        }
      }
    }
  }
  
private:
  std::atomic<bool> m_running;
  static const unsigned int event_head;
  std::vector<tcp::socket> m_inputs;
  std::shared_ptr<std::thread> m_thread;
  std::map<int, Built> m_events;
};

const unsigned int Builder::event_head = 0xAABBCCDD;

class DataServer : public Controlled {

public:
  DataServer(std::shared_ptr<Builder>& builder):
    m_io_service(),
    m_acceptor(m_io_service),
    m_running(false),
    m_builder(builder)
  {}

  virtual ~DataServer(){}
  
  void configure() {
    // Port 12345 is our listening point
    tcp::endpoint endpoint(tcp::v4(), 12345);
    m_acceptor.open(endpoint.protocol());

    // Black magic to avoid errors if a new 
    // instance is started immediately after a previous one
    // (I will be happy to explain this, but not to type it here)
    m_acceptor.set_option(tcp::acceptor::reuse_address(true));

    // Listen for incoming connections
    m_acceptor.bind(endpoint);

    m_acceptor.listen();

    m_thread = std::make_shared<std::thread>(&DataServer::work, this);
  }
  
  void unconfigure() {
    if (m_running) {
      m_running = false;
      m_thread->join();
      m_acceptor.close();
    }
  }
  
  void start() {
    if (m_running) {
      m_running = false;
      m_thread->join();
      m_acceptor.close();
    }
  }
  
  void stop() {
  }
  
private:
  
  void work() {
  
    // Timeout for poll operation
    int poll_timeout_ms = 1;

    // Descriptor for the poll operation
    // Instructs poll to check for available data on
    // on the acceptor socket
    struct pollfd pollfd;
    memset(&pollfd, 0, sizeof(pollfd));
    
    pollfd.fd = m_acceptor.native_handle();
    pollfd.events |= POLLIN;

    m_running = true;
    
    while(m_running) {
      
      // With poll we check if data are available
      // before reading
      // This is needed to avoid blocking forever
      if (poll(&pollfd, 1, poll_timeout_ms) > 0) {

        // Data are available. This is the server
        // so we expect this is a new incoming connection
        // We accept it and provide it to the builder
        tcp::socket socket(m_io_service);
        m_acceptor.accept(socket);
        m_builder->add_input(std::move(socket));
        std::cout << "New data connection received" << std::endl;
      }
    }
  }

private:
  boost::asio::io_service m_io_service;
  tcp::acceptor m_acceptor;
  std::atomic<bool> m_running;
  std::shared_ptr<std::thread> m_thread;
  std::shared_ptr<Builder> m_builder;
};

int main() {

  FSM& fsm = FSM::instance();
  
  auto builder = std::make_shared<Builder>();
  auto server = std::make_shared<DataServer>(builder);
    
  fsm.add_control(server);
  fsm.add_control(builder);
  
  fsm.run();
  
  return 0;
}
