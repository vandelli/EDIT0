
#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <iostream>

using boost::asio::ip::tcp;
namespace po = boost::program_options;

bool parse_args(int argc, char * argv[],
                po::variables_map & vm) {

  po::options_description desc("Options");
  desc.add_options()
    ("help", "Print help messages")
    ("servers,s", po::value<std::vector<std::string>>()->required(), "server name and port [name:port]")
    ;

  try {
    po::store(po::parse_command_line(argc, argv, desc),
              vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return false;
    }

    po::notify(vm);

  } catch(po::error& e) {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cerr << desc << std::endl;
    return false;
  }

  return true;
}

int main(int argc, char * argv[]) {

  po::variables_map vm;
  
  if (!parse_args(argc, argv, vm)) {
    return 1;
  }

  std::vector<std::string> servers = vm["servers"].as<std::vector<std::string>>();
  
  boost::asio::io_service io_service;
  std::vector<tcp::socket> controlled;
    
  for(const auto& s: servers) {
  
    auto server_name = s.substr(0, s.find(":"));
    auto port = s.substr(s.find(":")+1);
    
    // Find the server by name
    tcp::resolver resolver(io_service);
    tcp::resolver::query query(tcp::v4(), server_name, port);
    tcp::resolver::iterator iterator = resolver.resolve(query);
    tcp::resolver::iterator end;
    
    tcp::endpoint server;
    
    if (iterator != end) {
      server = *iterator;
    } else {
      std::cout << "Server not found " << server_name
                << " " << port << std::endl;
      return 1;
    }

    controlled.emplace_back(io_service);
    tcp::socket& socket = controlled.back();
    
    try {
      // Connect to the server
      socket.connect(server);
    } catch(boost::system::system_error &error) {
      std::cout << "Connect failed with: " << error.what()
                << " Server: "<< server_name
                << " " << port << std::endl;
      return 1;
    }

  }
    
  while(true) {

    std::string cmd;
    
    std::cout << "Next command: ";
    std::cin >> cmd;
    // Clear the return
    std::cin.get();

    for(auto& ctrl: controlled) {
      try {
        // Send the command
        boost::asio::write(ctrl, boost::asio::buffer(cmd));
        
        // Check if an errors comes back
            // Timeout for poll operation
        int poll_timeout_ms = 500;
        
        // Descriptor for the poll operation
        // Instructs poll to check for available data on
        // on the acceptor socket
        struct pollfd pollfd;
        memset(&pollfd, 0, sizeof(pollfd));
    
        pollfd.fd = ctrl.native_handle();
        pollfd.events |= POLLIN;

        if (poll(&pollfd, 1, poll_timeout_ms) > 0) {
          // An error was sent back
          size_t recv_size = 0;
          size_t buffer_size = 8192;
          char buffer[buffer_size];
          try {
            recv_size =
              ctrl.read_some(boost::asio::buffer(&buffer, buffer_size));
          } catch (boost::system::system_error &e) {
            break;
          }
          
          std::string error(buffer, recv_size);
          std::cout << "ERROR during transition from  "
                    << ctrl.remote_endpoint().address().to_string()
                    << ":" << ctrl.remote_endpoint().port()
                    << " Message: " << error << std::endl;
        }
        
      } catch (boost::system::system_error &e) {
        std::cout << "Something bad happened " << e.what() << std::endl;
        break;
      }
    }
  }
  
  return 0;
}
