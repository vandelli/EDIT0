
#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <iostream>

using boost::asio::ip::tcp;
namespace po = boost::program_options;

bool parse_args(int argc, char * argv[],
                po::variables_map & vm) {

  po::options_description desc("Options");
  desc.add_options()
    ("help", "Print help messages")
    ("server,s", po::value<std::string>()->required(), "server name")
    ("port,p", po::value<std::string>()->required(), "server port")
    ("data,d", po::value<unsigned int>()->required(), "repeated data word")
    ("length,l", po::value<unsigned int>()->required(), "data size (0 means random)")
    ("index,i", po::value<unsigned int>()->default_value(0), "start index")
    ;

  try {
    po::store(po::parse_command_line(argc, argv, desc),
              vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return false;
    }

    po::notify(vm);

  } catch(po::error& e) {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cerr << desc << std::endl;
    return false;
  }

  return true;
}

int main(int argc, char * argv[]) {

  po::variables_map vm;
  
  if (!parse_args(argc, argv, vm)) {
    return 1;
  }

  std::string server_name = vm["server"].as<std::string>();
  std::string port = vm["port"].as<std::string>();
  unsigned int data = vm["data"].as<unsigned int>();
  unsigned int length_w = vm["length"].as<unsigned int>();
  unsigned int length_b = length_w * sizeof(unsigned int);
  unsigned int start_idx = vm["index"].as<unsigned int>();

  boost::asio::io_service io_service;

  // Find the server by name
  tcp::resolver resolver(io_service);
  tcp::resolver::query query(tcp::v4(), server_name, port);
  tcp::resolver::iterator iterator = resolver.resolve(query);
  tcp::resolver::iterator end;

  tcp::endpoint server;
  
  if (iterator != end) {
    server = *iterator;
  } else {
    std::cout << "Server not found" << std::endl;
    return 1;
  }

  tcp::socket socket(io_service);

  try {
    // Connect to the server
    socket.connect(server);
  } catch(boost::system::system_error &error) {
    std::cout << "Connect failed with: " << error.what() << std::endl;
    return 1;
  }

  std::cout << "Connection successful. Press a key to start data flow."
            << std::endl;
  std::cin.ignore();

  std::unique_ptr<unsigned int[]> message(new unsigned int[length_w]);

  for(unsigned int i = 0; i < length_w; i++) {
    message[i] = data;
  }
  
  for(unsigned int sequence_index = start_idx;
      sequence_index < 0xffffffff; ++sequence_index) {

    try {
      // Send the length of the message
      boost::asio::write(socket, boost::asio::buffer(&length_b, sizeof(length_b)));

      // Send the sequence number
      boost::asio::write(socket, boost::asio::buffer(&sequence_index,
                                                     sizeof(sequence_index)));
      
      // Send the message
      boost::asio::write(socket, boost::asio::buffer(message.get(),
                                                     length_b));
    } catch (boost::system::system_error &e) {
      std::cout << "Something bad happened " << e.what() << std::endl;
      break;
    }

  }
  
  return 0;
}
