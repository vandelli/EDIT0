
#include <boost/asio.hpp>
#include <iostream>
#include <thread>
#include <vector>
#include <atomic>
#include <sys/socket.h>
#include <errno.h>
#include <poll.h>
#include <csignal>

using boost::asio::ip::tcp;

class InputHandler {

public:
  InputHandler(tcp::socket socket):
    m_socket(std::move(socket)),
    m_running(true),
    m_worker(&InputHandler::work, this)
  {};
  
  void stop() {
    m_running = false;
    m_worker.join();
  }
  
private:
  
  void work() {
    
    
    int poll_timeout_ms = 1;
    
    // Descriptor for the poll operation
    // Instructs poll to check for available data on
    // on the socket
    struct pollfd pollfd;
    memset(&pollfd, 0, sizeof(pollfd));
    
    pollfd.fd = m_socket.native_handle();
    pollfd.events |= POLLIN;
    
    while(m_running) {

      // With poll we check if data are available
      // before reading
      // This is needed to avoid blocking forever
      auto result = poll(&pollfd, 1, poll_timeout_ms);
      try {
        if (result == 0) {
          // No data available
          continue;
        } else if (result < 0) {
          // Some error occourred
          // Create a proper error
          int err_code = errno;
          boost::system::system_error error(err_code,
                                            boost::system::system_category());
          throw error;
        }
                
        // Receive the size of message
        unsigned int length;
        boost::asio::read(m_socket, boost::asio::buffer(&length, sizeof(length)));
        // Allocate the needed memory
        std::unique_ptr<char[]> buffer(new char[length]);
        
        // Receive the full message
        auto recv =
          boost::asio::read(m_socket,
                            boost::asio::buffer(buffer.get(), length));
        
        // Print the message
        std::cout << "Expected size: " << length
                  << " Received size: " << recv
                  << " Message: '" << std::string(buffer.get(), recv)
                  << "'" << std::endl;
        
      } catch (boost::system::system_error &e) {
        std::cout << "Something bad happened " << e.what() << std::endl;
        break;
      }
    }
  }

private:
  tcp::socket m_socket;
  std::atomic<bool> m_running;
  std::thread m_worker;
};


class DataServer {

public:
  DataServer():
    m_io_service(),
    m_acceptor(m_io_service),
    m_running(false)
  {
    // Port 12345 is our listening point
    tcp::endpoint endpoint(tcp::v4(), 12345);
    m_acceptor.open(endpoint.protocol());

    // Black magic to avoid errors if a new 
    // instance is started immediately after a previous one
    // (I will be happy to explain this, but not to type it here)
    m_acceptor.set_option(tcp::acceptor::reuse_address(true));

    // Listen for incoming connections
    m_acceptor.bind(endpoint);
    m_acceptor.listen();
  }

  
  void run() {

    // Timeout for poll operation
    int poll_timeout_ms = 1;

    // Descriptor for the poll operation
    // Instructs poll to check for available data on
    // on the acceptor socket
    struct pollfd pollfd;
    memset(&pollfd, 0, sizeof(pollfd));
    
    pollfd.fd = m_acceptor.native_handle();
    pollfd.events |= POLLIN;

    m_running = true;
    
    while(m_running) {
      
      // With poll we check if data are available
      // before reading
      // This is needed to avoid blocking forever
      if (poll(&pollfd, 1, poll_timeout_ms) > 0) {

        // Data are available. This is the server
        // so we expect this is in a new incoming connection
        // We accept it and spawn and handler for it
        tcp::socket socket(m_io_service);
        m_acceptor.accept(socket);
	m_workers.emplace_back(new InputHandler(std::move(socket)));
      }
    }

    std::cout << "Server done" << std::endl;
  }

  void stop() {
    // Stop accepting new connections
    m_running = false;

    // Stop the workers
    for(auto& w: m_workers) {
      w->stop();
    }

    // Delete all workers
    m_workers.clear();
  }

private:
  boost::asio::io_service m_io_service;
  tcp::acceptor m_acceptor;
  std::atomic<bool> m_running;
  std::vector<std::unique_ptr<InputHandler>> m_workers;
};


DataServer * server;

void signal_handler(int) {
  server->stop();
}

int main() {

  server = new DataServer;

  std::signal(SIGTERM, signal_handler);
  
  server->run();
  
  return 0;
}
